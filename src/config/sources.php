<?php

return [
    'wildberries' => [
        'parser_url' => env('SOURCES_WILDBERRIES_PARSER_URL'),
    ],
    'ozon' => [
        'parser_url' => env('SOURCES_OZON_PARSER_URL'),
    ]
];
