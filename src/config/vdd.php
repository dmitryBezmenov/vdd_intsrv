<?php

return [
    'api_base_uri' => env('VDD_API_BASE_URI'),
    'api_token' => env('VDD_API_TOKEN'),
];
