<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Item\Status;
use Inertia\Inertia;
use App\Models\VddItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Jobs\GenerateFeedJob;

class ExportController extends Controller
{
    public function index(Request $request)
    {
        $items = VddItem::query()->whereHas('item')->with('item');

        $status = $request->get('status');
        if (!empty($status) && $status != 'all') {
            $items = $items->filterByStatus(Status::fromString($status));
        }

        if ($title = $request->get('title')) {
            $items = $items->whereHas('item', fn($query) => $query
                ->where(DB::raw('lower(title)'), 'like', "%$title%")
            );
        }

        $items = $items->orderBy('id', 'asc');

        return Inertia::render('Export/Index', [
            'items' => $items->paginate(15),
        ]);
    }

    public function detail($item_id)
    {
        $vdd_item = VddItem::where('id', $item_id)->with('item')->first();

        if (!$vdd_item) {
            return response('', 404);
        }

        return Inertia::render('Export/Detail', [
            'item' => $vdd_item,
        ]);
    }

    public function toggle($item_id)
    {
        $vdd_item = VddItem::findOrFail($item_id);
        $vdd_item->active = !$vdd_item->active;
        $vdd_item->save();
        
        return back();
    }

    public function change($item_id, Request $request)
    {
        $vdd_item = VddItem::where('id', $item_id)->with('item')->first();

        if (!$vdd_item) {
            return response('', 404);
        }

        $new_item = $vdd_item->item->duplicates()->where('id', $new_item_id = $request->post('new_item_id'))->first();

        if (!$new_item) {
            return response('', 404);
        }

        $vdd_item->item->duplicates()->where('price', '>', 0)->whereNot('id', $new_item_id)->update(['duplicate_of' => $new_item->id]);
        $vdd_item->item->update(['duplicate_of' => $new_item->id, 'vdd_item_id' => null]);
        $new_item->update(['duplicate_of' => null, 'vdd_item_id' => $vdd_item->id]);

        return back();
    }

    public function generate_feed()
    {
        GenerateFeedJob::dispatch();

        return back();
    }
}
