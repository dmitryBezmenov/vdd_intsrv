<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class IndexController extends Controller
{
    public function index() 
    {
        if (Auth::user()) {
            return redirect(route('items'));
        }

        return redirect(route('login'));
    }
}
