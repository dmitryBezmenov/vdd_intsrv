<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\VddItem;
use App\Models\VddItemImage;
use App\Services\ImageService;

class ImagesController extends Controller
{
    public function select($item_id, Request $request)
    {
        $vdd_item = VddItem::where('id', $item_id)->first();

        if (!$vdd_item) {
            return response('', 404);
        }

        $vdd_item->selected_images = (object)$request->post('map');
        $vdd_item->save();

        return response();
    }

    public function cut($item_id, $index, Request $request)
    {
        $vdd_item = VddItem::where('id', $item_id)->first();

        if (!$vdd_item) {
            return response('', 404);
        }

        $image = new VddItemImage();
        $image->vdd_item_id = $vdd_item->id;
        $image->source = $vdd_item->all_images[$index];
        $image->save();

        if (filter_var($image->source, FILTER_VALIDATE_URL) || !file_exists($image->source)) {
            file_put_contents($image->getPath(), file_get_contents($image->source));
        }
        
        $service = new ImageService($image);

        try {
            $service->cutObject();
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        return response()->json([
            'error' => false, 
            'image' => [
                'url' => $image->getPart(),
                'id' => $image->id,
            ]
        ]);
    }

    public function remove($image_id)
    {
        VddItemImage::findOrFail($image_id)->delete();
    }
}
