<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Item;
use App\Models\Item\Status;
use App\Models\Item\Source;
use App\Models\VddItem;
use Illuminate\Support\Facades\DB;

class ItemsController extends Controller
{
    public function index(Request $request)
    {
        $items = Item::query();

        $source = $request->get('source');
        if (!empty($source) && $source != 'all') {
            $items = $items->where('source', $source);
        }

        $status = $request->get('status');
        if (!empty($status) && $status != 'all') {
            $items = $items->filterByStatus(Status::fromString($status));
        }

        $has_ikea_vendor_code = $request->get('has_ikea_vendor_code');
        if (!empty($has_ikea_vendor_code) && $has_ikea_vendor_code != 'all') {
            if ($has_ikea_vendor_code == 'yes') {
                $items = $items->where('ikea_vendor_code', '!=', '');
            } else {
                $items = $items->where('ikea_vendor_code', '');
            }
        }

        $title = mb_strtolower($request->get('title'));
        if (!empty($title)) {
            $items = $items->where(DB::raw('lower(title)'), 'like', "%$title%");
        }

        $items = $items->parents()
            ->with('vdd_item', 'duplicates')
            ->orderBy('title', 'asc')
            ->paginate(10);

        return Inertia::render('Items/Index', [
            'items' => $items,
            'sources' => Source::cases(),
        ]);
    }

    public function detail(int $item_id)
    {
        $item = Item::where('id', $item_id)->with('duplicates', 'vdd_item', 'parent.vdd_item')->first();

        if (!$item) {
            return response('', 404);
        }

        return Inertia::render('Items/Detail', [
            'item' => $item,
        ]);
    }

    public function toggle_export(int $item_id)
    {
        $item = Item::where('id', $item_id)->with('vdd_item')->first();

        if (!$item) {
            return response('', 404);
        }

        if (!$item->vdd_item) {
            $item->vdd_item_id = VddItem::create([
                'selected_images' => (object)array_map(fn($i) => true, $item->images)
            ])->id;
            $item->save();
            return back();
        }

        $item->vdd_item->active = !$item->vdd_item->active;
        $item->vdd_item->save();

        return back();
    }

    public function merge(int $item_id, Request $request)
    {
        Item::findOrFail($item_id);

        $items = array_filter($request->post('items'), fn($id) => $id != $item_id);

        Item::whereIn('id', $items)->update(['duplicate_of' => $item_id]);

        return back();
    }

    public function update_ikea_vendor_code(int $item_id, Request $request)
    {
        $item = Item::findOrFail($item_id);
        $item->ikea_vendor_code = $request->post('ikea_vendor_code');
        $item->save();

        return back();
    }
}
