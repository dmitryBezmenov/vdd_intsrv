<?php

namespace App\Models\Item;

enum Status: string 
{
    case New = 'new';
    case Exported = 'exported';
    case Inactive = 'inactive';

    public static function fromString(string $status): Status
    {
        foreach (static::cases() as $case) {
            if ($status == $case->value) {
                return $case;
            }
        }

        throw new \Exception('invalid status ' . $status);
    }
}
