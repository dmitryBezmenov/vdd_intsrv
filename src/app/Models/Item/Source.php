<?php

namespace App\Models\Item;

enum Source: string {
    case Wildberries = 'wildberries';
    case Ozon = 'ozon';
}
