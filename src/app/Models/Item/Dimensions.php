<?php

namespace App\Models\Item;

class Dimensions
{
    public function __construct(
        public float $length,
        public float $width,
        public float $height,
    ) {}
}
