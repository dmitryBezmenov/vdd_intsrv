<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VddIkeaItem extends Model
{
    use HasFactory;

    public $timestamps = false;

    public $fillable = [
        'vendor_code',
        'name',
    ];

    public $appends = [
        'vendor_name_en',
        'vendor_name_ru',
    ];

    public function getVendorNameEnAttribute(): ?string
    {
        if (count($parts = explode(' ', $this->name)) < 3 || !preg_match('/[a-z]/i', $first = $parts[0]) || strtoupper($first) != $first) {
            return null;
        }

        return $first;
    }

    public function getVendorNameRuAttribute(): ?string
    {
        if (count($parts = explode(' ', $this->name)) < 3 || preg_match('/[a-z]/i', $second = $parts[1]) || mb_strtoupper($second) != $second) {
            return null;
        }

        return $second;
    }
}
