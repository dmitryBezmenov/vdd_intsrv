<?php

namespace App\Models;

use App\Models\Item\Dimensions;
use App\Models\Item\Source;
use App\Models\Item\Status;
use App\Models\VddItem;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Item extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = [
        'external_id',
        'source',
        'url',
        'category',
        'title',
        'price',
        'color',
        'structrure',
        'order_count',
        'attributes',
        'images',
        'duplicate_of',
        'vdd_item_id',
        'description',
        'brand',
        'delivery_days',
        'delivery_seller_store',
        'vdd_ikea_item_id',
    ];

    public $casts = [
        'attributes' => 'array',
        'images' => 'array',
        'source' => Source::class,
    ];

    public function duplicates()
    {
        return $this->hasMany(Item::class, 'duplicate_of', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(Item::class, 'duplicate_of', 'id');
    }

    public function vdd_item()
    {
        return $this->belongsTo(VddItem::class, 'vdd_item_id', 'id');
    }

    public function vdd_ikea_item()
    {
        return $this->belongsTo(VddIkeaItem::class, 'ikea_vendor_code', 'vendor_code');
    }

    public function scopeParents(Builder $builder)
    {
        return $builder->whereNull('duplicate_of');
    }

    public function scopeFilterByStatus(Builder $builder, Status $status)
    {
        return match($status) {
            Status::New => $builder->whereNull('vdd_item_id'),
            Status::Exported => $builder->whereNotNull('vdd_item_id')->whereHas('vdd_item', function($query) {
                $query->where('active', true);
            }),
            Status::Inactive => $builder->whereNotNull('vdd_item_id')->whereHas('vdd_item', function($query) {
                $query->where('active', false);
            }),
        };
    }

    // public function getPackDimensions(): Dimensions
    // {
    //     $attrs = collect(json_decode($this->attributes['attributes'], true));
        
    //     $length = $attrs->filter(fn($attr) => $attr['name'] == 'Длина упаковки')->pop();
    //     $length = $length ? intval($length['value']) : 0;

    //     $width = $attrs->filter(fn($attr) => $attr['name'] == 'Ширина упаковки')->pop();
    //     $width = $width ? intval($width['value']) : 0;

    //     $height = $attrs->filter(fn($attr) => $attr['name'] == 'Высота упаковки')->pop();
    //     $height = $height ? intval($height['value']) : 0;

    //     return new Dimensions($length, $width, $height);
    // }

    // public function getPackWeight(): float
    // {
    //     $attrs = collect(json_decode($this->attributes['attributes'], true));

    //     $weight = $attrs->filter(fn($attr) => $attr['name'] == 'Вес с упаковкой (кг)')->pop();
    //     $weight = $weight ? floatval($weight['value']) : 0;

    //     return $weight;
    // }
}
