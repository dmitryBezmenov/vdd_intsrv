<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VddItemImage extends Model
{
    use HasFactory;

    public $fillable = [
        'vdd_item_id',
        'source',
    ];

    public $appends = [
        'url'
    ];

    public function vdd_item()
    {
        return $this->belongsTo(VddItem::class, 'vdd_item_id', 'id');
    }

    public function getPart()
    {
        return "/images/" . md5($this->id) . ".jpg";
    }

    public function getUrl()
    {
        return env('APP_URL') . $this->getPart();
    }

    public function getPath()
    {
        return dirname(__FILE__, 3)  . '/public' . $this->getPart();
    }
    
    protected static function boot() 
    {
        parent::boot();
        static::deleting(function(VddItemImage $image) {
            if (file_exists($image->getPath())) {
                unlink($image->getPath());
            }
        });
    }

    public function getUrlAttribute()
    {
        return $this->getUrl();
    }
}
