<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Item\Status;

class VddItem extends Model
{
    use HasFactory;

    public $fillable = [
        'active',
        'selected_images',
    ];

    public $casts = [
        'selected_images' => 'object',
    ];

    public $appends = [
        'sku',
        'other_items',
        'all_images',
    ];

    public function item()
    {
        return $this->hasOne(Item::class, 'vdd_item_id', 'id');
    }

    public function images()
    {
        return $this->hasMany(VddItemImage::class, 'vdd_item_id', 'id');
    }

    public function getOtherItemsAttribute()
    {
        return $this->item->duplicates()->where('price', '>', 0)->get();
    }

    public function getSkuAttribute()
    {
        return 'VDM' . str_repeat('0', 6-strlen((string)$this->id)) . $this->id;
    }

    public function getAllImagesAttribute()
    {
        $images = [];

        foreach ($this->images as $image) {
            $images[] = $image->url;
        }

        foreach ($this->item->images as $url) {
            $images[] = $url;
        }

        foreach ($this->other_items as $other) {
            foreach ($other->images as $url) {
                $images[] = $url;
            }
        }

        return $images;
    }

    public function scopeFilterByStatus(Builder $builder, Status $status)
    {
        return match($status) {
            Status::Exported => $builder->where('active', true),
            Status::Inactive => $builder->where('active', false),
        };
    }
}
