<?php

namespace App\Services;

use GuzzleHttp\Client;

class VddApiService
{
    private Client $httpClient;

    public function __construct() 
    {
        $this->httpClient = new Client([
            'base_uri' => rtrim(config('vdd.api_base_uri'), '/'),
            'headers' => [
                'Authorization' => 'Bearer ' . config('vdd.api_token'),
                'Accept' => 'application/json',
            ],
            'timeout' => 30,
        ]);
    }

    public function sendExternalSupplierFeed(string $url)
    {
        return $this->httpClient->post('/api/v1/external-supplier/feed', [
            'json' => [
                'feed_url' => $url
            ]
        ]);
    }
}
