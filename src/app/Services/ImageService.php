<?php

namespace App\Services;

use App\Models\VddItemImage;

class ImageService
{
    public function __construct(
        public VddItemImage $image
    ) {}

    public function cutObject()
    {
        $target = $this->image->getPath();
        $png = substr($target, 0, -3) . "png";

        if (exec("python3 -m carvekit -i $target -o $png --post fba --trimap_dilation 3 --trimap_erosion 0") === false) {
            throw new \Exception('failed to exec bg removing tool');
        }

        unlink($target);

        if (exec("convert -flatten -background white $png $target") === false) {
            throw new \Exception('failed to exec convert');
        }

        unlink($png);
    }
}
