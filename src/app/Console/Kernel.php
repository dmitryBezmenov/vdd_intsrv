<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(Commands\Wildberries\LoadFromParser::class)->dailyAt("06:00");
        $schedule->command(Commands\Ozon\LoadFromParser::class)->dailyAt("06:00");
        // $schedule->command(Commands\Vdd\GenerateFeed::class)->hourly();
        // $schedule->command(Commands\Vdd\LoadIkeaItems::class)->dailyAt("07:00");
        // $schedule->command(Commands\Vdd\MergeItemsByIkea::class)->dailyAt("07:10");
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
