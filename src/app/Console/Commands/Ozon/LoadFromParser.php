<?php

namespace App\Console\Commands\Ozon;

use App\Models\Item;
use App\Models\Item\Source;
use Illuminate\Console\Command;

class LoadFromParser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ozon:load-from-parser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $info = file_get_contents(config('sources.ozon.parser_url') . "/feed.json");

        if (empty($info) || empty($data = json_decode($info, true))) {
            $this->error("failed to get feed info");
            return 1;
        }

        $ids_in_feed = [];

        foreach ($data['contents'] as $chunk_url) {
            
            $chunk = file_get_contents(config('sources.ozon.parser_url') . "/$chunk_url");
            
            if (empty($chunk)) {
                $this->error("empty chunk $chunk_url");
            }

            $items = new \SimpleXMLElement($chunk);

            foreach ($items as $item) {

                if (intval($item->price) == 0 || empty($item->title)) {
                    continue;
                }

                $ids_in_feed[] = $item->id;

                $attributes = [];

                if ($item->attributes->attribute->count() > 0) {
                    foreach ($item->attributes->attribute as $attribute) {
                        $attributes[] = [
                            'name' => (string)$attribute->name,
                            'value' => (string)$attribute->value
                        ];
                    }
                }
                
                $images = [];
            
                if ($item->images->image->count() > 0) {
                    foreach ($item->images->image as $image) {
                        $images[] = (string)$image;
                    }
                }

                $parent = Item::updateOrCreate([
                    'external_id' => $item->id,
                    'source' => Source::Ozon,
                ], [
                    'url' => $item->url,
                    'title' => $item->title,
                    'category' => $item->category,
                    'price' => $item->price,
                    'color' => $item->color,
                    'structure' => $item->structure,
                    'attributes' => $attributes,
                    'images' => $images,
                    'description' => $item->description,
                    'brand' => $item->brand,
                    'delivery_days' => $item->delivery['days'] ?? -1,
                ]);

                if ($item->duplicates->duplicate->count() > 0) {
                    foreach ($item->duplicates as $duplicate) {
                        dd($duplicate->count());
                        $duplicate = $duplicate->duplicate;
    
                        $ids_in_feed[] = $duplicate->id;
    
                        $images = [];
                        foreach ($duplicate->images->image as $image) {
                            $images[] = (string)$image;
                        }
    
                        Item::updateOrCreate([
                            'external_id' => $duplicate->id,
                            'source' => Source::Ozon,
                        ], [
                            'url' => $duplicate->url,
                            'images' => $images,
                            'duplicate_of' => $parent->id,
                            'attributes' => [],
                        ]);
                    }
                }                
            }
        }

        $this->output->writeln(count($ids_in_feed));

        Item::whereNotIn('external_id', $ids_in_feed)->where('source', Source::Ozon)->delete();
    }
}
