<?php

namespace App\Console\Commands\Vdd;

use Illuminate\Console\Command;
use App\Jobs\GenerateFeedJob;

class GenerateFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vdd:generate-feed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        GenerateFeedJob::dispatchSync();
    }
}
