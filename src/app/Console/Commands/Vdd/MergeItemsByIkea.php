<?php

namespace App\Console\Commands\Vdd;

use Illuminate\Console\Command;
use App\Models\VddIkeaItem;
use App\Models\Item;
use App\Models\VddItem;

class MergeItemsByIkea extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vdd:merge-items-by-ikea';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $vdd_ikea_items = VddIkeaItem::all();

        $vdd_ikea_items->each(function($vdd_ikea_item) {

            $pattern = "";

            if ($vdd_ikea_item->vendor_name_en) {
                $pattern .= "\\\\b$vdd_ikea_item->vendor_name_en\\\\b";
            }

            if ($vdd_ikea_item->vendor_name_ru) {
                if (!empty($pattern)) {
                    $pattern .= "|";
                }
                $pattern .= "\\\\b$vdd_ikea_item->vendor_name_ru\\\\b";
            }

            if ($vdd_ikea_item->vendor_code) {
                if (!empty($pattern)) {
                    $pattern .= "|";
                }
                $pattern .= "\\\\b$vdd_ikea_item->vendor_code\\\\b";
            }

            $items = Item::whereRaw(<<<SQL
            upper(replace(replace(concat(`title`, ' ', `description`, ' ', `attributes`), '.', ''), '-', ''))
            regexp 
            '{$pattern}'
            SQL)->where('ikea_vendor_code', '');

            if ($items->count() < 2) {
                return;
            }

            $origin = $items->clone()
                ->has('duplicates')
                ->whereNull('duplicate_of')
                ->first();

            if (!$origin) {
                $origin = $items->clone()
                    ->whereNull('duplicate_of')
                    ->first();
            }

            if (!$origin) {
                return;
            }

            $update_ids = $items->clone()
                ->where('id', '!=', $origin->id)
                ->doesnthave('duplicates')
                ->whereNull('duplicate_of')
                ->get()
                ->pluck('id');

            Item::whereIn('id', $update_ids)->update(['duplicate_of' => $origin->id, 'vdd_item_id' => null]);
            VddItem::doesnthave('item')->delete();

            $items->clone()->update(['ikea_vendor_code' => $vdd_ikea_item->vendor_code]);
        });
    }
}
