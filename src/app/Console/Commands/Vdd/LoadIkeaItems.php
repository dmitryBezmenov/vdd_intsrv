<?php

namespace App\Console\Commands\Vdd;

use App\Models\VddIkeaItem;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class LoadIkeaItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vdd:load-ikea-items';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = file_get_contents("https://vamdodoma.ru/api/v1/product?brand=ikea&per_page=100000");
        $data = json_decode($data, true);

        if (empty($data) || !key_exists('data', $data)) {
            Log::error('vdd api not working as expected');
            return 1;
        }

        $data = $data['data'];
        $ids = [];

        foreach ($data as $item) {
            if (mb_strpos(mb_strtolower($item['name']), 'аналог') !== false) {
                continue;
            }
            if (mb_strpos(mb_strtolower($item['name']), 'икеа') !== false) {
                continue;
            }
            if (strpos(strtolower($item['name']), 'ikea') !== false) {
                continue;
            }

            $ids[] = VddIkeaItem::updateOrCreate([
                'vendor_code' => $item['vendor_code'],
                'name' => $item['name'],
            ])->id;
        }

        VddIkeaItem::whereNotIn('id', $ids)->delete();
    }
}
