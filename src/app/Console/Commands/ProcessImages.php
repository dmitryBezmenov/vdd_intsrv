<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\VddItemImage;
use App\Models\VddItem;
use App\Services\ImageService;

class ProcessImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $vdd_items = VddItem::orderBy('updated_at', 'asc')->has('item')->with('item');
        $vdd_items->offset(0)->limit(5)->each(function($vdd_item) {
            foreach ($vdd_item->item?->images as $source) {
                if (VddItemImage::where('source', $source)->count() > 0) {
                    continue;
                }

                $image = new VddItemImage();
                $image->vdd_item_id = $vdd_item->id;
                $image->source = $source;
                $image->save();
                
                if (filter_var($source, FILTER_VALIDATE_URL) || !file_exists($image->getPath())) {
                    file_put_contents($image->getPath(), file_get_contents($source));
                }
    
                $service = new ImageService($image);
                $service->cutObject();

                $vdd_item->touch();
            }
        });
    }
}
