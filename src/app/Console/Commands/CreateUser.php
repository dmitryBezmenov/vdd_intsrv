<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->ask('email');
        $password = $this->ask('password');

        $user = User::create([
            'name' => $email,
            'email' => $email,
            'password' => bcrypt($password),
        ]);

        $this->output->writeln("done! id {$user->id}");
    }
}
