<?php

namespace App\Jobs;

use App\Models\Item;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Bukashk0zzz\YmlGenerator\Settings;
use App\Services\Feed\Generator;
use Bukashk0zzz\YmlGenerator\Model\ShopInfo;
use Bukashk0zzz\YmlGenerator\Model\Offer\OfferSimple;
use Bukashk0zzz\YmlGenerator\Model\Offer\OfferParam;
use App\Services\VddApiService;
use Illuminate\Support\Facades\Log;

class GenerateFeedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $generator = new Generator(
            (new Settings())->setOutputFile(dirname(__FILE__, 3) . '/public/catalog.yml')->setEncoding('UTF-8')
        );

        $offers = [];

        Item::chunk(100, function($chunk) use (&$offers) 
        {
            foreach ($chunk as $item) 
            {
                $offer = new OfferSimple();
                $offer->setAvailable(true);
                $offer->setUrl($item->url);
                $offer->setName($item->title);
                $offer->setVendor($item->brand);
                $offer->setVendorCode($item->external_id);
                $offer->setDescription($item->description);
                
                $offer->addCustomElement('brand', $item->brand);

                foreach ($item->attributes as $attribute) {
                    $param = new OfferParam();
                    $param->setName($attribute['name']);
                    $param->setValue($attribute['value']);
                    $offer->addParam($param);
                }

                $price = $item->duplicates()->where('price', '>', 0)->orderBy('price', 'asc')->first()?->price ?? PHP_INT_MAX;
                $price = min($item->price, $price);

                $offer->setPrice($price/100);

                foreach ($item->images as $url) {
                    $offer->addPicture($url);   
                }

                $offers[] = $offer;
            }
        });

        $generator->generate(new ShopInfo(), [], [], $offers, []);

        $vddApi = new VddApiService();
        try {
            $vddApi->sendExternalSupplierFeed(config('app.url') . '/catalog.yml');
        } catch (\Exception $e) {
            Log::error('external supplier feed was not sent: ' . $e->getMessage());
        }
    }
}
