<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Controllers\IndexController::class, 'index'])->name('index');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {

    Route::group(['prefix' => 'items'], function() {
        Route::get('/', [Controllers\ItemsController::class, 'index'])->name('items');
        Route::get('/{item_id}', [Controllers\ItemsController::class, 'detail'])->name('items.detail');
        Route::post('/{item_id}/toggle_export', [Controllers\ItemsController::class, 'toggle_export'])->name('items.toggle_export');
        Route::post('/{item_id}/merge', [Controllers\ItemsController::class, 'merge'])->name('items.merge');
        Route::post('/{item_id}/update_ikea_vendor_code', [Controllers\ItemsController::class, 'update_ikea_vendor_code'])->name('items.update_ikea_vendor_code');
    });

    Route::group(['prefix' => 'export'], function() {
        Route::get('/', [Controllers\ExportController::class, 'index'])->name('export');
        Route::post('/generate_feed', [Controllers\ExportController::class, 'generate_feed'])->name('export.generate_feed');
        Route::get('/{item_id}', [Controllers\ExportController::class, 'detail'])->name('export.detail');
        Route::post('/{item_id}/toggle', [Controllers\ExportController::class, 'toggle'])->name('export.toggle');
        Route::post('/{item_id}/change', [Controllers\ExportController::class, 'change'])->name('export.change');

        Route::group(['prefix' => 'images'], function() {
            Route::post('/{item_id}/select', [Controllers\Export\ImagesController::class, 'select'])->name('export.images.select');
            Route::post('/{item_id}/cut/{index}', [Controllers\Export\ImagesController::class, 'cut'])->name('export.images.cut');
            Route::post('/{image_id}/remove', [Controllers\Export\ImagesController::class, 'remove'])->name('export.images.remove');
        });
    });
});
