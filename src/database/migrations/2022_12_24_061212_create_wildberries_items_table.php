<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wildberries_items', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('wb_id');
            $table->string('url')->nullable();
            $table->string('title')->nullable();
            $table->float('price')->nullable();
            $table->string('color')->nullable();
            $table->string('structrure')->nullable();
            $table->integer('order_count')->nullable();
            $table->json('attributes');
            $table->json('images');
            $table->foreignId('duplicate_of')->nullable()->references('id')->on('wildberries_items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wildberries_items');
    }
};
